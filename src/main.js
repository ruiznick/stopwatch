
function StopWatch() {
    this.isRunning = false
    this.currentTime = {hours: 0, minutes: 0, seconds: 0}

    var that = this

    this.getCurrentTime = function () {
        var cTime = that.currentTime.hours < 10 ? "0"+that.currentTime.hours : that.currentTime.hours
        cTime += ":"
        cTime += that.currentTime.minutes < 10 ? "0"+that.currentTime.minutes : that.currentTime.minutes
        cTime += ":"
        cTime += that.currentTime.seconds < 10 ? "0"+that.currentTime.seconds : that.currentTime.seconds
        return cTime
    }

    // Toggle the stopwatch
    this.startPause = function() {
        that.isRunning = !that.isRunning

        if (that.isRunning) {
            document.getElementById("start").innerHTML = "Pause"
        } else {
            document.getElementById("start").innerHTML = "Start"
        }
    }

    this.incrementTime = function() {
        that.currentTime.seconds++

        if (that.currentTime.seconds > 59) {
            that.currentTime.seconds = 0
            that.currentTime.minutes++

            if (that.currentTime.minutes > 59) {
                that.currentTime.minutes = 0
                that.currentTime.hours++
            }

        }
    }

    this.resetWatch = function() {
        that.currentTime = {hours: 0, minutes: 0, seconds: 0}
        that.isRunning = true
        that.startPause()
    }
}

var myWatch

function ready() {
    myWatch = new StopWatch()

    document.getElementById("lap").addEventListener("click", recordLap)
    document.getElementById("start").addEventListener("click", myWatch.startPause)
    document.getElementById("reset").addEventListener("click", resetWatch)

    myWatch.secondsEl = document.getElementById("seconds")
    myWatch.minutesEl = document.getElementById("minutes")
    myWatch.hoursEl = document.getElementById("hours")

    initInterval()
}

function resetWatch() {
    clearInterval(myWatch.intervalID)
    myWatch.resetWatch()
    myWatch.secondsEl.innerHTML = "00"
    myWatch.minutesEl.innerHTML = "00"
    myWatch.hoursEl.innerHTML = "00"

    document.getElementById("laps").innerHTML = ""

    initInterval()
}

function recordLap() {
    var newLap = "<li>" + myWatch.getCurrentTime() + "</li>"

    document.getElementById("laps").innerHTML += newLap
}

function initInterval() {
    // Update the time
    myWatch.intervalID = setInterval( function() {
        if (!myWatch.isRunning) {
            return
        }

        myWatch.incrementTime()

        var currentTime = myWatch.getCurrentTime().split(":")
        myWatch.secondsEl.innerHTML = currentTime[2]
        myWatch.minutesEl.innerHTML = currentTime[1]
        myWatch.hoursEl.innerHTML = currentTime[0]

    }, 1000)
}

if (document.readyState !== "loading") {
    ready()
} else {
    document.addEventListener("DOMContentLoaded", ready)
}
